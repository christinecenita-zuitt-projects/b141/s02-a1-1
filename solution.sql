Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 111
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> CREATE TABLE users (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> email VARCHAR(100) NOT NULL,
    -> password VARCHAR(50) NOT NULL,
    -> datetime_created DATETIME NOT NULL,
    -> PRIMARY KEY(id)
    -> );
Query OK, 0 rows affected (0.874 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| users             |
+-------------------+
1 row in set (0.002 sec)

MariaDB [blog_db]> DESCRIBE users;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| email            | varchar(100) | NO   |     | NULL    |                |
| password         | varchar(50)  | NO   |     | NULL    |                |
| datetime_created | datetime     | NO   |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
4 rows in set (0.261 sec)

MariaDB [blog_db]> CREATE TABLE posts (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> author_id INT NOT NULL,
    -> title VARCHAR(200) NOT NULL,
    -> content VARCHAR(5000) NOT NULL,
    -> datetime_posted DATETIME NOT NULL,
    -> PRIMARY KEY (id),
    -> CONSTRAINT fk_posts_user_id
    -> FOREIGN KEY (author_id) REFERENCES users (id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (0.519 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
| users             |
+-------------------+
2 rows in set (0.001 sec)

MariaDB [blog_db]> DESCRIBE posts;
+-----------------+---------------+------+-----+---------+----------------+
| Field           | Type          | Null | Key | Default | Extra          |
+-----------------+---------------+------+-----+---------+----------------+
| id              | int(11)       | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)       | NO   | MUL | NULL    |                |
| title           | varchar(200)  | NO   |     | NULL    |                |
| content         | varchar(5000) | NO   |     | NULL    |                |
| datetime_posted | datetime      | NO   |     | NULL    |                |
+-----------------+---------------+------+-----+---------+----------------+
5 rows in set (0.117 sec)

MariaDB [blog_db]> CREATE TABLE post_likes (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> user_id INT NOT NULL,
    -> post_id INT NOT NULL,
    -> datetime_liked DATETIME NOT NULL,
    -> PRIMARY KEY (id),
    -> CONSTRAINT fk_post_likes_user_id
    -> FOREIGN KEY (user_id) REFERENCES users(id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT,
    -> CONSTRAINT fk_post_likes_post_id
    -> FOREIGN KEY (post_id) REFERENCES posts(id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (0.545 sec)

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 112
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_likes        |
| posts             |
| users             |
+-------------------+
3 rows in set (0.001 sec)

MariaDB [blog_db]> DESCRIBE post_likes;
+----------------+----------+------+-----+---------+----------------+
| Field          | Type     | Null | Key | Default | Extra          |
+----------------+----------+------+-----+---------+----------------+
| id             | int(11)  | NO   | PRI | NULL    | auto_increment |
| user_id        | int(11)  | NO   | MUL | NULL    |                |
| post_id        | int(11)  | NO   | MUL | NULL    |                |
| datetime_liked | datetime | NO   |     | NULL    |                |
+----------------+----------+------+-----+---------+----------------+
4 rows in set (0.061 sec)

MariaDB [blog_db]> CREATE TABLE post_comments (
    ->     id INT NOT NULL AUTO_INCREMENT,
    ->     user_id INT NOT NULL,
    ->     post_id INT NOT NULL,
    ->     content VARCHAR(5000) NOT NULL,
    ->     datetime_commented DATETIME NOT NULL,
    ->     PRIMARY KEY (id),
    ->     CONSTRAINT fk_post_comments_user_id
    ->     FOREIGN KEY (user_id) REFERENCES users(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT,
    ->     CONSTRAINT fk_post_comments_post_id
    ->     FOREIGN KEY (post_id) REFERENCES posts (id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    ->     );
Query OK, 0 rows affected (0.750 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.002 sec)

MariaDB [blog_db]> DESCRIBE post_comments;
+--------------------+---------------+------+-----+---------+----------------+
| Field              | Type          | Null | Key | Default | Extra          |
+--------------------+---------------+------+-----+---------+----------------+
| id                 | int(11)       | NO   | PRI | NULL    | auto_increment |
| user_id            | int(11)       | NO   | MUL | NULL    |                |
| post_id            | int(11)       | NO   | MUL | NULL    |                |
| content            | varchar(5000) | NO   |     | NULL    |                |
| datetime_commented | datetime      | NO   |     | NULL    |                |
+--------------------+---------------+------+-----+---------+----------------+
5 rows in set (0.188 sec)

MariaDB [blog_db]>


